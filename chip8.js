DBG             = true;
MEMORY_SIZE     = 4096
STACK_SIZE      = 16
FONT_SIZE       = 80
HEIGHT          = 64
WIDTH           = 32
REGISTER_COUNT  = 16
KEY_COUNT       = 16

var opcode;     // Stores the current opcode
var memory;     // Array used to replicate chip8 memory
var V;          // Array of registers
var stack;      // Array used as a stack
var I;          // Index register
var pc;         // Program counter
var delay_timer;
var sound_timer;// Emits a sound when counts down to 0
var sp;         // Stack pointer
var gfx;        // Framebuffer
var image;
var framebuffer;
var key;        // Array of states for each key
var drawFlag;   // Indicates if we should draw anything

var fontset = new Array(
    0xF0, 0x90, 0x90, 0x90, 0xF0,   // 0
    0x20, 0x60, 0x20, 0x20, 0x70,   // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0,   // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0,   // 3
    0x90, 0x90, 0xF0, 0x10, 0x10,   // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0,   // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0,   // 6
    0xF0, 0x10, 0x20, 0x40, 0x40,   // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0,   // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0,   // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90,   // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0,   // B
    0xF0, 0x80, 0x80, 0x80, 0xF0,   // C
    0xE0, 0x90, 0x90, 0x90, 0xE0,   // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0,   // E
    0xF0, 0x80, 0xF0, 0x80, 0x80    // F
);

// Object to hold the display data
// TODO: Figure out best way to create references from the Framebuffer
// 		to the display buffer using the Pixels object, (or one like it)
function Pixels(length) {
	this.data = new Array(length);
	for(var i = 0; i < length; i++) {
		this.data[i] = 0;
	}
}

function init() {
    opcode  = 0;        // Reset current opcode
    pc      = 0x200;    // Chip8 apps start at this address
    I       = 0;        // Reset index register
    sp      = 0;        // Reset stack pointer

    // Initialize the memory
    memory = new Array(MEMORY_SIZE);
    for(i = 0; i < MEMORY_SIZE; ++i)
        memory[i] = 0;

    // Initialize the registers
    V = new Array(REGISTER_COUNT);
    for(i = 0; i < REGISTER_COUNT; ++i)
        V[i] = 0;

    // Initialize the stack
    stack = new Array(STACK_SIZE);
    for(i = 0; i < STACK_SIZE; ++i)
        stack[i] = 0;

    // Initialize the graphics
    //gfx = new Array(HEIGHT * WIDTH);
    //for(i = 0; i < HEIGHT * WIDTH; ++i)
        //gfx[i] = 0;
    gfx = new Pixels(HEIGHT * WIDTH);
        
    for(i = 0; i < framebuffer.length; i++) {
		framebuffer[i] = 255;
	}

    // Initialize the keys
    key = new Array(KEY_COUNT);
    for(i = 0; i < KEY_COUNT; ++i)
        key[i] = 0;

    // Load the fontset into memory
    for(i = 0; i < FONT_SIZE; ++i)
        memory[i] = fontset[i];

    // Reset the timers
    delay_timer = 0;
    sound_timer = 0;

    // Clear the screen once
    //drawFlag = true;
    display();
}

function loadGame(game) {
    init();

    if(DBG)
        logd("Loading " + game.name + "...");

    if(!(window.File && window.FileReader && window.Blob)) {
        loge("Your browswer does not support the required operations");
        return;
    }
    
    // Read each byte from the file
    var reader = new FileReader();
    reader.onload = function(e) {
        var result = e.target.result;
        
        // If we have enough room to, load each byte into memory
        if((MEMORY_SIZE - 512) > result.length) {
            for(i = 0; i < result.length; ++i) {
                memory[i + 512] = result.charCodeAt(i);
            }
            if(DBG) {
                logd(game.name + " loaded");
                logd("Filesize: " + result.length);
            }
        } else {
            loge("Cannot load application, not enough memory");
        }
    }

    // Handle any errors reading the file
    reader.onerror = function(e) {
        switch(e.target.error.code) {
            case e.target.error.NOT_FOUND_ERR:
                loge("File not found!");
                break;
            case e.target.error.NOT_READABLE_ERR:
                loge("File is not readable");
                break;
            default:
                loge("Error reading file");
                break;
        }
    }
    
    // Start reading the file
    reader.readAsBinaryString(game);
}


function step() {
    // Opcode is 2 bytes, so grab the current and next byte for full value
    opcode = memory[pc] << 8 | memory[pc + 1];

    // Start decoding the opcodes, then execute them
    switch(opcode & 0xF000) {
        case 0x0000:
            switch(opcode & 0x000F) {
                case 0x0000:    // 00E0: Clears the screen
                    for(i = 0; i < HEIGHT * WIDTH; ++i) 
                        gfx.data[i] = 0;
                    //drawFlag = true;
                    display();
                    pc += 2;
                break;
                case 0x000E:        // 00EE: Return from subroutine
                    --sp;           // Go back one in the stack
                    pc = stack[sp]; // Load the return location
                    pc += 2;        // Continue from the return location
                break;
                default:
                    if(DBG)
                        logd("Unknown 0x0000 opcode: " + opcode.toString(16));
                break;
            }
        break;
        case 0x1000:    // 1NNN: Jumps to address at NNN
            pc = (opcode & 0x0FFF);
        break;
        case 0x2000:    // 3NNN: Calls subroutine at NNN
            stack[sp] = pc;
            ++sp;
            pc = (opcode & 0x0FFF);
        break;
        case 0x3000:    // 3XNN: Skips next instruction if VX equals NN
            if(V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
                pc += 4;
            else
                pc += 2;
        break;
        case 0x4000:    // 4XNN: Skips the next instruction if VX doesn't equal NN
            if(V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
                pc += 4;
            else
                pc += 2;
        break;
        case 0x5000:    // 5XY0: Skips the next instruction if VX equals VY
            if(V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
                pc += 4;
            else
                pc += 2;
        break;
        case 0x6000:    // 6XNN: Sets VX to NN
            V[(opcode & 0x0F00) >> 8] = (opcode & 0x00FF);
            pc += 2;
        break;
        case 0x7000:    // 7XNN: Adds NN to VX
            V[(opcode & 0x0F00) >> 8] += (opcode & 0x00FF);
            pc += 2;
        break;
        case 0x8000:
            switch(opcode & 0x000F) {
                case 0x0000:    // 8XY0: Sets VX to value of VY
                    V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                break;
                case 0x0001:    // 8XY1: Sets VX to VX OR VY
                    V[(opcode & 0x0F00) >> 8] |= V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                break;
                case 0x0002:    // 8XY2: Sets VX to VX AND VY
                    V[(opcode & 0x0F00) >> 8] &= V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                break;
                case 0x0003:    // 8XY3: Sets VX to VX XOR VY
                    V[(opcode & 0x0F00) >> 8] ^= V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                break;
                case 0x0004:    // 8XY4: Adds VY to VX
                    // VF gets set to 1 if there is a carry
                    // VF gets set to 0 when there is no carry
                    if(V[(opcode & 0x00F0) >> 4] > (0xFF - (V[(opcode & 0x0F00) >> 8])))
                        V[0xF] = 1; // carry
                    else
                        V[0xF] = 0; // no carry
                    V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                break;
                case 0x0005:    // 8XY5: Subtracts VY from VX
                    // VF gets set to 0 when theres a borrow
                    // VF gets set to 1 when there isn't a borrow
                    if(V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
                        V[0xF] = 0; // borrow
                    else
                        V[0xF] = 1; // no borrow
                    V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
                    pc += 2;
                break;
                case 0x0006:    // 8XY6: Shifts VX right by one
                    // VF is set to the value of the LSB of VX before the shift
                    V[0xF] = V[(opcode & 0x0F00) >> 8] & 0x1;
                    V[(opcode & 0x0F00) >> 8] >>= 1;
                    pc += 2;
                break;
                case 0x0007:    // 8XY7: Sets VX to VY minus VX
                    // VF gets set to 0 if there is a borrow, 1 if not
                    if(V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])
                        V[0xF] = 0; // borrow
                    else
                        V[0xF] = 1; // no borrow
                    V[(opcode & 0x0F00) >> 8] = 
                        V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                break;
                case 0x000E:    // 8XYE: Shifts VS left by 1
                    // VF is set to the MSB of VX before the shift
                    V[0xF] = V[(opcode & 0x0F00) >> 8] >> 7;
                    V[(opcode & 0x0F00) >> 8] <<= 1;
                    pc += 2;
                break;
                default:
                    if(DBG)
                        logd("Unhandled 0x8000 opcode: " + opcode.toString(16));
                break;
            }
        break;
        case 0x9000:    // 9XY0: Skips the next instruction if VX doesn't equal VY
            if(V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
                pc += 4;
            else 
                pc += 2;
        break;
        case 0xA000:    // ANNN: Sets I to the address of NNN
            I = (opcode & 0x0FFF);
            pc += 2;
        break;
        case 0xB000:    // BNNN: Jumps to the address of NNN plus V0
            pc = (opcode & 0x0FFF) + V[0];
        break;
        case 0xC000:    // CXNN: Sets VX to a random number and NN
            V[(opcode & 0x0F00) >> 8] = (Math.floor(Math.random() % 0xFF)) & (opcode & 0x00FF);
            pc += 2;
        break;
        case 0xD000:    // DXYN: Draws a sprite at VX, VY, with width = 8, height = n
        {
            // Each row of pixels is read as bit-coded starting from memory location I
            // I value doesn't change after the exeuction of the instruction
            // VF is set to 1 if any screen pixels are flipped from set to unset
            // while the sprite is draw, and 0 if that doens't happen
            var x = V[(opcode & 0x0F00) >> 8];
            var y = V[(opcode & 0x00F0) >> 4];
            var height = (opcode & 0x000F);
            var pixel;
            var spot = 0;
            var index = 0;
            var on = 255;

            V[0xF] = 0; // Only set to 1 if we flip a pixel

            for(yline = 0; yline < height; yline++){
                pixel = memory[I + yline];

                for(xline = 0; xline < 8; xline++) {
                    // Check each new pixel to see if they are "on"
                    if((pixel & (0x80 >> xline)) != 0){
                        // If the existing pixel is 1, flag a collision
                        if(gfx.data[(x + xline + (( y + yline) * HEIGHT))] == 1) {
                            V[0xF] = 1;
                        }
                        // Set the new value of the pixel with a XOR
                        spot = x + xline + ((y + yline) * HEIGHT);
                        gfx.data[spot] ^= 1;
      					
      					// TODO: Update onced the displaybuffer/framebuffer system is finalized
                        scaleImage(gfx.data, 64, 32, 10, 10, framebuffer);
                    }
                }
            }
            //drawFlag = true;
            display();
            pc += 2;
        }
        break;
        case 0xE000:
            switch(opcode & 0x00FF) {
                case 0x009E:    // EX9E: Skips the next instruction if the key at VX is pressed
                    if(key[V[(opcode & 0x0F00) >> 8]] != 0)
                        pc +=4;
                    else
                        pc += 2;
                break;
                case 0x00A1:    // EXA1: Skipps the next instruction if the key at VX isn't pressed
                    if(key[V[(opcode & 0x0F00) >> 8]] == 0) 
                        pc += 4;
                    else
                        pc += 2;
                break;
                default:
                    if(DBG)
                        logd("Unhandled 0xE000 opcode " + opcode.toString(16));
                break;
            }
        break;
        case 0xF000:
            switch (opcode & 0x00FF) {
                case 0x0007:    // FX07: Sets VX to the value of the delay time
                    V[(opcode & 0x0F00) >> 8] = delay_timer;
                    pc += 2;
                break;
                case 0x000A:    // FX0A: A key press is awaited, and then stored in VX
                {
                    var keyPress = false;

                    for(i = 0; i < KEY_COUNT; ++i) {
                        if(key[i] != 0) {
                            V[(opcode & 0x0F00) >> 8] = i;
                            keyPress = true;
                        }
                    }

                    // No key press occured
                    if (!keyPress)
                        return;

                    pc += 2;
                }
                break;
                case 0x0015:    // FX15: Sets the delay timer to VX
                    delay_timer = V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                break;
                case 0x0018:    // FX18: Sets the sound timer to VX
                    sound_timer = V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                break;
                case 0x001E:    // FX1E: Adds VX to I
                    I += V[(opcode & 0x0F00) >> 8];
                    pc += 2;
                break;
                case 0x0029:    // FX29: Sets I to the location of the sprite for the character in VX
                    // Characters 0-F are represented by a 4x5 font (see fontset)
                    // I = the spot in the fontset where the character at VX starts
                    I = V[(opcode & 0x0F00) >> 8] * 0x5;
                    pc += 2;
                break;
                case 0x0033:    // FX33: Stores binary represenation of VX in memory
                    // Most significate of three digitis at the address in I
                    // The middle digit at I plus 1, and the least significate at I + 2
                    memory[I] = V[(opcode & 0x0F00) >> 8] / 100;
                    memory[I + 1] = (V[(opcode & 0x0F00) >> 8] / 10) % 10;
                    memory[I + 2] = (V[(opcode & 0x0F00) >> 8] % 100) % 10;
                    pc += 2;
                break;
                case 0x0055:    // FX55: Stores V0 to VX in memory address starting at I
                    for(i = 0; i <= ((opcode & 0x0F00) >> 8); ++i) {
                        memory[I + i] = V[i];
                    }

                    // When done, we need to increment I (I = I + X + 1)
                    I += ((opcode & 0x0F00) >> 8) + 1;
                    pc += 2;
                break;
                case 0x0065:    // FX65: Fills V0 to VX with values from memory starting at I
                    for(i = 0; i <= ((opcode & 0x0F00) >> 8); ++i) {
                        V[i] = memory[I + i];
                    }

                    // When done, increment I (I = I + X + 1)
                    I += ((opcode * 0x0F00) >> 8) + 1;
                    pc += 2;
                break;
                default:
                    if (DBG)
                        logd("Unhandled 0xF000 opcode " + opcode.toString(16));
                break;
            }
        break;
        default:
            if (DBG)
                logd("Unhandled opcode " + opcode.toString(16));
        break;
    } 
    

}

function updateTimers() {
	    // Update timers
    if(delay_timer > 0)
        --delay_timer;
    if(sound_timer > 0) {
        if(sound_timer == 1)
            doBeep();
        --sound_timer;
    }
}
function dumpState() {
	logd("Stack:");
	for(i = 0; i < STACK_SIZE; ++i)
		logd(i + ": " + stack[i]);
	logd("Registers: ")
	for(i = 0; i < REGISTER_COUNT; ++i)
		logd("V" + i + ": " + V[i]);
}
function doBeep() {
    logd("Beep!");
}

function logd(msg) {
    if(window.console && console.log)
        console.log(msg);
}

function loge(msg) {
    logd(msg);
}
